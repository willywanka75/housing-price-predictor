# Nathan Lenz
# Software Engineering GUI

import tkinter as tk
from tkinter import ttk
from tkinter import filedialog
from Controller import Controller
import locale

HEADER_FONT = ("Veranda", 15, "bold")
pages = ["Price", "Date", "Bedrooms", "Bathrooms", "Square Footage", "Lot Square Footage", "Floors", "Waterfront",
           "View", "Condition of House", "Grade of House", "Square Footage of Upper Floors", "Square Footage of Base",
           "Year Built", "Year Renovated", "Zip Code", "Latitude", "Longitude", "Square Footage of 15 closest Houses",
           "Square Footage of 15 Closest Lots"]
description = ["greater than 1", "if the format \"YYYY-MM-DD\" and 2014 or later", "at least 0", "at least 0", "at least 1", "at least 1", "at least 1",
               "a 0 or a 1", "at least 0 and at most 4", "at least 1 and at most 5", "at least 1 and at most 13",
               "at least 1", "at least 0", "at least 1900", "0 or at least 1900", "one of the 52 zip codes in King County",
               "at least 47 and at most 48", "at least -123 and at most -121", "at least 1", "at least 1"]

class GUI(tk.Tk):

    def __init__(self):
        tk.Tk.__init__(self)
        self.title("Housing Price Predictor")
        self.geometry("650x250+300+300")

        self.controller = Controller()
        
        mainFrame = ttk.Frame(self)
        mainFrame.pack(side="top", fill="both", expand=True)
        mainFrame.grid_rowconfigure(0, weight=1)
        mainFrame.grid_columnconfigure(0, weight=1)

        self.frames = {}
        for page in pages:
            frame = InputFrames(mainFrame, self, page)
            self.frames[page] = frame
            frame.grid(row=0, column=0, sticky="nsew")
        self.frames["date"] = InputFrames(mainFrame, self, "date")
        self.frames["date"].grid(row=0, column=0, sticky="nsew")
        self.frames["square Footage of 15 Closest Lots"] = InputFrames(mainFrame, self, "square Footage of 15 Closest Lots")
        self.frames["square Footage of 15 Closest Lots"].grid(row=0, column=0, sticky="nsew")
        self.frames["MainMenu"] = MainMenu(mainFrame, self)
        self.frames["MainMenu"].grid(row=0, column=0, sticky="nsew")
        self.frames["Predict"] = Predict(mainFrame, self)
        self.frames["Predict"].grid(row=0, column=0, sticky="nsew")
        self.frames["Retrain"] = Retrain(mainFrame, self)
        self.frames["Retrain"].grid(row=0, column=0, sticky="nsew")
        self.frames["PredictBulk"] = PredictBulk(mainFrame, self)
        self.frames["PredictBulk"].grid(row=0, column=0, sticky="nsew")
        self.frames["AddBulk"] = AddBulk(mainFrame, self)
        self.frames["AddBulk"].grid(row=0, column=0, sticky="nsew")
        self.frames["Error"] = AddBulk(mainFrame, self)
        self.frames["Error"].grid(row=0, column=0, sticky="nsew")

        self.predicting = None
        self.showFrame("MainMenu")

    def showFrame(self, page, predict=None):
        if page == "Predict":
            self.frames["Predict"].predict(self)
        if page == "Retrain":
            if not self.controller.writeData():
                page = "Error"
        if predict != None:
            self.predicting = predict
        if self.predicting and page == "Date":
            page = "date"
        elif not self.predicting and page == "date":
            page = "Date"
        elif self.predicting and page == "Square Footage of 15 Closest Lots":
            page = "square Footage of 15 Closest Lots"

        if page == "MainMenu":
            for frame in self.frames.values():
                try:
                    frame.entry.delete(0, "end")
                except AttributeError:
                    pass

        frame = self.frames[page]
        if page in pages or page == "date" or page == "square Footage of 15 Closest Lots":
            frame.setFocus()
        frame.tkraise()

class MainMenu(tk.Frame):

    def __init__(self, parent, mainFrame):
        ttk.Frame.__init__(self, parent)
        
        label = tk.Label(self, text="Housing Price Predictor", font=HEADER_FONT)
        label.pack(side="top", pady=10)

        leftFrame = ttk.Frame(self)
        leftFrame.pack(pady=15)
        predictButton = ttk.Button(leftFrame, text="Predict a Price", command=lambda: mainFrame.showFrame("date", predict = True))
        predictButton.pack(side="left", padx=2)
        predictBulkButton = ttk.Button(leftFrame, text="Predict a Price from CSV File", command=lambda: mainFrame.showFrame("PredictBulk"))
        predictBulkButton.pack(side="left", padx=2)

        rightFrame = ttk.Frame(self)
        rightFrame.pack(pady=15)
        addButton = ttk.Button(rightFrame, text="Add to Dataset", command=lambda: mainFrame.showFrame("Price", predict = False))
        addButton.pack(side="left", padx=2)
        predictBulkButton = ttk.Button(rightFrame, text="Add to Dataset from CSV File", command=lambda: mainFrame.showFrame("AddBulk"))
        predictBulkButton.pack(side="left", padx=2)

class InputFrames(tk.Frame):

    def __init__(self, parent, mainFrame, name):
        ttk.Frame.__init__(self, parent)

        backFrame = "MainMenu"
        nextFrame = "Retrain"
        if name == "date":
            index = 1
        elif name == "square Footage of 15 Closest Lots":
            index = len(pages)-1
        else:
            index = pages.index(name)
        if index != 0:
            backFrame = pages[index-1]
        if index != len(pages)-1:
            nextFrame = pages[index+1]
        elif name == "square Footage of 15 Closest Lots":
            nextFrame = "Predict"
        
        self.label = tk.Label(self, text="Enter " + name[:1].upper() + name[1:], font=HEADER_FONT)
        self.label.pack(side="top", pady=10)
        self.errorLabel = tk.Label(self, text="Must be " + description[index])
        self.errorLabel.pack(side="top", pady=10)
        self.entry = ttk.Entry(self)
        self.entry.pack(pady=10)

        bottomFrame = ttk.Frame(self)
        bottomFrame.pack(pady=10)
        if name != "Price" and name != "date":
            backButton = ttk.Button(bottomFrame, text="Back", command=lambda: mainFrame.showFrame(backFrame))
            backButton.pack(side="left", padx=2)
        mmButton = ttk.Button(bottomFrame, text="Main Menu", command=lambda: mainFrame.showFrame("MainMenu"))
        mmButton.pack(side = "left", padx=2)
        nextButton = ttk.Button(bottomFrame, text="Next", command=lambda:self.checkValidity(mainFrame, name, nextFrame))
        nextButton.pack(side="left", padx=2)

    def setFocus(self): 
        self.entry.focus_set()

    def checkValidity(self, mainFrame, name, nextFrame):
        if mainFrame.controller.checkValidity(name, self.entry.get()):
           self.errorLabel.config(fg="black")
           mainFrame.showFrame(nextFrame)
        else:
           self.errorLabel.config(fg="red")

class Predict(tk.Frame):
    def __init__(self, parent, mainFrame):
        ttk.Frame.__init__(self, parent)
        
        title = tk.Label(self, text="Housing Price Predictor", font=HEADER_FONT)
        title.pack(side="top", pady=10)

        self.price = tk.Label(self, text="", font=("Veranda", 12, "bold"))
        self.price.pack(side="top", pady=10)

        price = tk.Label(self, text="Suggestion: Renovate the house to increase the value by $78,273")
        price.pack(side="top", pady=10)
        
        mmButton = ttk.Button(self, text="Main Menu", command=lambda: mainFrame.showFrame("MainMenu"))
        mmButton.pack(pady=10)

    def predict(self, mainFrame):
        locale.setlocale(locale.LC_ALL, '')
        self.price.config(text="$" + locale.format("%d", mainFrame.controller.predict(), grouping = True))
        self.update_idletasks()


class Error(tk.Frame):
    def __init__(self, parent, mainFrame):
        ttk.Frame.__init__(self, parent)

        title = tk.Label(self, text="Error!", font=HEADER_FONT)
        title.pack(side="top", pady=10)

        self.price = tk.Label(self, text="Something when wrong when writing to data set.\nCheck your file.", font=("Veranda", 12, "bold"))
        self.price.pack(side="top", pady=10)

        mmButton = ttk.Button(self, text="Main Menu", command=lambda: mainFrame.showFrame("MainMenu"))
        mmButton.pack(pady=10)

class Retrain(tk.Frame):
    def __init__(self, parent, mainFrame):
        ttk.Frame.__init__(self, parent)
        self.mainFrame = mainFrame
        
        title = tk.Label(self, text="Retrain", font=HEADER_FONT)
        title.pack(side="top", pady=10)

        self.progressLabel = tk.Label(self, text="Ready")
        self.progressLabel.pack(side="top", pady=5)

        retrain = ttk.Button(self, text="Retrain", command=self.updateProgress)
        retrain.pack(pady=10)
        
        mmButton = ttk.Button(self, text="Main Menu", command=self.changeFrame)
        mmButton.pack(pady=10)

    def updateProgress(self):
        self.mainFrame.controller.retrain()
        self.progressLabel.config(text="Done!")
        self.mainFrame.update_idletasks()

    def changeFrame(self):
        self.progressLabel.config(text="Ready")
        self.mainFrame.update_idletasks()
        self.mainFrame.showFrame("MainMenu")

class PredictBulk(tk.Frame):
    def __init__(self, parent, mainFrame):
        ttk.Frame.__init__(self, parent)
        self.mainFrame = mainFrame

        title = tk.Label(self, text="Predict From a CSV File", font=HEADER_FONT)
        title.pack(side="top", pady=10)

        self.info = tk.Label(self, text="Output will be in a txt file in the same directory as the CSV")
        self.info.pack(side="top", pady=10)

        #path = filedialog.askopenfilename()

        bottomFrame = ttk.Frame(self)
        bottomFrame.pack(pady=10)
        mmButton = ttk.Button(bottomFrame, text="Main Menu", command=self.changeFrame)
        mmButton.pack(side = "left", padx=2)
        backButton = ttk.Button(bottomFrame, text="Import", command=filedialog.askopenfilename)
        backButton.pack(side="left", padx=2)
        nextButton = ttk.Button(bottomFrame, text="Bulk Predict", command=lambda: self.info.config(text="Done!"))
        nextButton.pack(side="left", padx=2)

    def changeFrame(self):
        self.info.config(text="Output will be in a txt file in the same directory as the CSV")
        self.mainFrame.update_idletasks()
        self.mainFrame.showFrame("MainMenu")

class AddBulk(tk.Frame):
    def __init__(self, parent, mainFrame):
        ttk.Frame.__init__(self, parent)
        self.mainFrame = mainFrame
        
        title = tk.Label(self, text="Add Data From a CSV File", font=HEADER_FONT)
        title.pack(side="top", pady=10)

        self.info = tk.Label(self, text="")
        self.info.pack(side="top", pady=5)

        #path = filedialog.askopenfilename()

        bottomFrame = ttk.Frame(self)
        bottomFrame.pack(pady=10)
        mmButton = ttk.Button(bottomFrame, text="Main Menu", command=self.changeFrame)
        mmButton.pack(side = "left", padx=2)
        backButton = ttk.Button(bottomFrame, text="Import", command=filedialog.askopenfilename)
        backButton.pack(side="left", padx=2)
        nextButton = ttk.Button(bottomFrame, text="Bulk Add", command=lambda: self.info.config(text="Done!"))
        nextButton.pack(side="left", padx=2)

    def changeFrame(self):
        self.info.config(text="")
        self.mainFrame.update_idletasks()
        self.mainFrame.showFrame("MainMenu")

pred = GUI()
pred.mainloop()
