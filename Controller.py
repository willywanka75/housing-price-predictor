# Nathan Lenz
# Software Engineering Controller

from Validate import Valid
from LRLeastSquares import Predict
from FileWriter import FileWriter

class Controller:
    def __init__(self):
        #file = "test_kc_house_data.csv"
        file = "kc_house_data.csv"
        self.valid = Valid()
        self.pred = Predict(file)
        self.writer = FileWriter(file)

    def checkValidity(self, name, text):
        if name.lower() == "price":
            return self.valid.checkPrice(name, text)
        elif name.lower() == "date":
            return self.valid.checkDate(name, text)
        elif name.lower() == "bedrooms":
            return self.valid.checkBedrooms(name, text)
        elif name.lower() == "bathrooms":
            return self.valid.checkBathrooms(name, text)
        elif name.lower() == "square footage":
            return self.valid.checkSquareFootage(name, text)
        elif name.lower() == "lot square footage":
            return self.valid.checkLotSquareFootage(name, text)
        elif name.lower() == "floors":
            return self.valid.checkFloors(name, text)
        elif name.lower() == "waterfront":
            return self.valid.checkWaterfront(name, text)
        elif name.lower() == "view":
            return self.valid.checkView(name, text)
        elif name.lower() == "condition of house":
            return self.valid.checkCondition(name, text)
        elif name.lower() == "grade of house":
            return self.valid.checkGrade(name, text)
        elif name.lower() == "square footage of upper floors":
            return self.valid.checkSquareFootageUpperFloors(name, text)
        elif name.lower() == "square footage of base":
            return self.valid.checkSquareFootageBase(name, text)
        elif name.lower() == "year built":
            return self.valid.checkYearBuilt(name, text)
        elif name.lower() == "year renovated":
            return self.valid.checkYearRenovated(name, text)
        elif name.lower() == "zip code":
            return self.valid.checkZipCode(name, text)
        elif name.lower() == "latitude":
            return self.valid.checkLatitude(name, text)
        elif name.lower() == "longitude":
            return self.valid.checkLongitude(name, text)
        elif name.lower() == "square footage of 15 closest houses":
            return self.valid.checkSquareFootageClosestHouses(name, text)
        elif name.lower() == "square footage of 15 closest lots":
            return self.valid.checkSquareFootageClosestLots(name, text)
        else:
            return False

    def predict(self):
        return self.pred.predict(self.valid.getValuesPredict())

    def writeData(self):
        return self.writer.writeOutList(self.valid.getValuesAdd())

    def retrain(self):
        self.pred.retrain()