Housing Price Predictor

-----------------------------------------------------------------------------------------------
About:

Predicts the price of a house based on 19 variables that are given.

The included dataset is from kaggle.com and is housing data from King Country, Washington.

-----------------------------------------------------------------------------------------------
Prerequisites:

1. The "kc_house_data.csv" that is supplied (or your own data that is in the same format).

2. Numpy

3. Python 3.x

-----------------------------------------------------------------------------------------------
Run in Linux:

python3 HousingPricePredictor.py

-----------------------------------------------------------------------------------------------
Algorithm:

A step-by-step description of the algorithm is described in Word document included in the project.

-----------------------------------------------------------------------------------------------
Notes:

The GUI is quite ugly and poorly set up. If I had to do this project again, I would use a different library for the GUI. The reason I used tkinter is because it is built into python.

Let me know if you see any issues with how I have implemented the prediction algorithm.
