import numpy as np

class Predict:
    def __init__(self, fileName):
        self.fileName = fileName
        self.retrain()

    def retrain(self):
        self.x, self.y = self.getXandY()
        self.b = self.linearLeastSquares(self.x, self.y)
        self.error = self.findError()

    # Read in CSV file
    def readInCSV(self):
        data = np.recfromcsv(self.fileName)
        return data

    # Get the price out of the data
    def getXandY(self):
        # Get data from file
        dataAsList = self.readInCSV()

        # Transpose data and get number of entries
        transposedData = list(zip(*dataAsList))
        self.m = len(transposedData[0])

        # Convert to Numpy array
        transposedWithY = np.asarray(transposedData, dtype=np.float32)

        # Get price (y_data) and delete it from x_data
        y_data = np.reshape(transposedWithY[1, :], (self.m, 1))
        transposed_x_data = np.delete(transposedWithY, 1, 0)

        # Re-transpose x_data
        x_data = np.transpose(transposed_x_data)

        return x_data, y_data

    # Calculate the coefficient vector Beta-hat
    def linearLeastSquares(self, X, Y):
        # Betahat = (X^T X)^-1 X^T y
        return np.linalg.inv(X.T.dot(X)).dot(X.T).dot(Y)

    # Predict for given inputs based on coefficents b
    def predict(self, x):
        y = 0
        for i in range(len(x)):
            y += x[i] * self.b[i,0]
        return round(y + self.error, 2)

    # Find epsilon for each
    def findError(self):
        error = 0
        for i in range(self.m):
            prediction = self.predictForError(self.x[i, :])
            diff = self.y[i, 0] - prediction
            error += diff
        return error / self.m

    # Predict without average error to find error.
    def predictForError(self, x):
        y = 0
        for i in range(len(x)):
            y += x[i] * self.b[i,0]
        return y

#    # Improves results slightly. Found from some analysis.
#    def fixErrorQuadratic(self, price):
#        a = 0.0000006344180807302181
#        b = -0.23637433125117557
#        c = 234463.64396145218
#        return a * (price ** 2) + b * price + c + price

if __name__ == "__main__":
    predictor = Predict()

    sum = 0
    diffTotal = 0
    for i in range(predictor.m):
        prediction = predictor.predict(predictor.x[i,:])
        diff = predictor.y[i,0] - prediction
        sum += diff
        diffTotal += abs(diff)
        print(prediction, predictor.y[i,0], diff)
        #time.sleep(3)

    print(sum / predictor.m)
    print(diffTotal / predictor.m)