# Nathan Lenz
# Software Engineering Predictor

from datetime import datetime

class Valid:
    def __init__(self):
        self.zipCodes = {
            "98002":True,"98071":True,"98092":True,"98224":True,"98005":True,
            "98008":True,"98009":True,"98015":True,"98041":True,"98070":True,
            "98019":True,"98052":True,"98122":True,"98007":True,"98024":True,
            "98001":True,"98003":True,"98023":True,"98063":True,"98093":True,
            "98155":True,"98038":True,"98103":True,"98288":True,"98056":True,
            "98025":True,"98011":True,"98119":True,"98029":True,"98075":True,
            "98034":True,"98051":True,"98028":True,"98030":True,"98031":True,
            "98032":True,"98035":True,"98064":True,"98089":True,"98033":True,
            "98083":True,"98125":True,"98014":True,"98042":True,"98112":True,
            "98199":True,"98039":True,"98040":True,"98010":True,"98059":True,
            "98006":True,"98166":True,"98045":True,"98047":True,"98027":True,
            "98050":True,"98191":True,"98053":True,"98073":True,"98054":True,
            "98129":True,"98055":True,"98057":True,"98058":True,"98160":True,
            "98177":True,"98133":True,"98185":True,"98074":True,"98062":True,
            "98102":True,"98104":True,"98106":True,"98107":True,"98109":True,
            "98111":True,"98113":True,"98114":True,"98116":True,"98117":True,
            "98118":True,"98121":True,"98124":True,"98127":True,"98131":True,
            "98132":True,"98134":True,"98139":True,"98141":True,"98144":True,
            "98145":True,"98148":True,"98154":True,"98158":True,"98161":True,
            "98164":True,"98165":True,"98170":True,"98171":True,"98174":True,
            "98175":True,"98181":True,"98184":True,"98190":True,"98194":True,
            "98198":True,"98178":True,"98065":True,"98101":True,"98138":True,
            "98168":True,"98188":True,"98105":True,"98195":True,"98013":True,
            "98022":True,"98115":True,"98136":True,"98126":True,"98146":True,
            "98072":True,"98077":True,"98004":True}
        self.values = [None] * 20

    def checkPrice(self, name, text):
        try:
            val = int(text)
        except ValueError:
            return False

        if val < 1:
            return False

        self.values[1] = val
        return True

    def checkDate(self, name, text):
        try:
            val = datetime.strptime(text, "%Y-%m-%d")
        except ValueError:
            return False

        if val.year < 2014:
            return False

        numDate = val.year * 10000 + val.month * 100 + val.day
        self.values[0] = numDate
        return True

    def checkBedrooms(self, name, text):
        try:
            val = int(text)
        except ValueError:
            return False

        if val < 0:
            return False

        self.values[2] = val
        return True

    def checkBathrooms(self, name, text):
        try:
            val = float(text)
        except ValueError:
            return False

        if val < 0:
            return False

        self.values[3] = val
        return True

    def checkSquareFootage(self, name, text):
        try:
            val = int(text)
        except ValueError:
            return False

        if val < 1:
            return False

        self.values[4] = val
        return True

    def checkLotSquareFootage(self, name, text):
        try:
            val = int(text)
        except ValueError:
            return False

        if val < 1:
            return False

        self.values[5] = val
        return True

    def checkFloors(self, name, text):
        try:
            val = float(text)
        except ValueError:
            return False

        if val < 1:
            return False

        self.values[6] = val
        return True

    def checkWaterfront(self, name, text):
        try:
            val = int(text)
        except ValueError:
            return False

        if val != 0 and val != 1:
            return False

        self.values[7] = val
        return True

    def checkView(self, name, text):
        try:
            val = int(text)
        except ValueError:
            return False

        if val < 0 or val > 4:
            return False

        self.values[8] = val
        return True

    def checkCondition(self, name, text):
        try:
            val = int(text)
        except ValueError:
            return False

        if val < 1 or val > 5:
            return False

        self.values[9] = val
        return True

    def checkGrade(self, name, text):
        try:
            val = int(text)
        except ValueError:
            return False

        if val < 1 or val > 13:
            return False

        self.values[10] = val
        return True

    def checkSquareFootageUpperFloors(self, name, text):
        try:
            val = int(text)
        except ValueError:
            return False

        if val < 1:
            return False

        self.values[11] = val
        return True

    def checkSquareFootageBase(self, name, text):
        try:
            val = int(text)
        except ValueError:
            return False

        if val < 0:
            return False

        self.values[12] = val
        return True

    def checkYearBuilt(self, name, text):
        try:
            val = int(text)
        except ValueError:
            return False

        if val < 1900:
            return False

        self.values[13] = val
        return True

    def checkYearRenovated(self, name, text):
        try:
            val = int(text)
        except ValueError:
            return False

        if val < 1900 and val != 0:
            return False

        self.values[14] = val
        return True

    def checkZipCode(self, name, text):
        try:
            val = int(text)
        except ValueError:
            return False

        if text not in self.zipCodes:
            return False

        self.values[15] = val
        return True

    def checkLatitude(self, name, text):
        try:
            val = float(text)
        except ValueError:
            return False

        if val < 47 or val > 48:
            return False

        self.values[16] = val
        return True

    def checkLongitude(self, name, text):
        try:
            val = float(text)
        except ValueError:
            return False

        if val < -123 or val > -121:
            return False

        self.values[17] = val
        return True

    def checkSquareFootageClosestHouses(self, name, text):
        try:
            val = int(text)
        except ValueError:
            return False

        if val < 1:
            return False

        self.values[18] = val
        return True

    def checkSquareFootageClosestLots(self, name, text):
        try:
            val = int(text)
        except ValueError:
            return False

        if val < 1:
            return False

        self.values[19] = val
        return True

    def getValuesPredict(self):
        temp = []
        for i in range(19):
            if i != 1:
                temp.append(self.values[i])

        return temp

    def getValuesAdd(self):
        return self.values