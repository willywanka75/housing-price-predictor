# Nathan Lenz
# Append data to a file

class FileWriter:
    def __init__(self, file):
        self.fileName = file

    def writeOutList(self, list):
        line = "\n"
        for i in range(len(list)):
            line += str(list[i]) + ","
        try:
            #with open("kc_house_data.csv", "a") as file:
            with open(self.fileName, "a") as file:
                file.write(line[:-1])
        except IOError:
            return False

        return True